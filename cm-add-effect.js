/***
 * 
 * !!cm-add-effect (--name="Unknown" --duration="1" --increment="-1"|--round|--set-macro)
 *
 * Adds an effect to the end of the turn tracker. The name, duration, and 
 * increment values are optional, but if they are omitted the values presented
 * in the usage will be used instead. If the "round" option is used a round
 * counter will be added to the end of the turn order.
 * 
 * The "set-macro" option will create a macro named "Add-Effect" which will
 * prompt you for the values of duration, name, and increment. It will also
 * add in a macros named "Add-Round-Counter"
 * 
 */

/* globals state on findObjs filterObjs _ log Campaign createObj sendChat */
(function(state){
    
    var defaultMacros = [
        {
            name: 'Add-Effect',
            action: '!!cm-add-effect --name=?{Name|Unknown} --duration=?{Duration|1} --increment=?{Increment|-1}'
        },
        {
            name: 'Add-Round-Counter',
            action: '!!cm-add-effect --round'
        }
    ];

    function between(_min, _max, _inclusive) {
        var min = _min || 0, 
            max = _max || _min + 1, 
            inclusive = _inclusive;
        
        if (max < min) {
            max = min + 1;
        }
        
        var methods = {
            min: function(value) {
                min = isNaN(value)?0:value;
                return methods;
            },
            max: function(value) {
                max = isNaN(value)?1:value;
                return methods;
            },
            inclusive: function(flag) {
                inclusive = flag;
                return methods;
            },
            value: function(value, default_value) {
                if (isNaN(value)) {
                    if (isNaN(default_value)) {
                        throw new TypeError('Non-number passed to between and no default value given.');
                    }
                    
                    return default_value;
                }
                
                if (inclusive === true) {
                    if (value < min) {
                        return min;
                    }
                    
                    if (value > max) {
                        return max;
                    }
                    return value;
                }
                
                if (value <= min) {
                    return min + 1;
                }
                
                if (value >= max) {
                    return max - 1;
                }
                return value;
            }
        };
        
        return methods;
    }

    function TurnSequencer(campaign) {
        if (typeof campaign !== 'function') {
            throw new TypeError('TurnSequencer contructor error: Campaign object is not a function. Don\'t forget to provide the Campaign object as the first parameter');
        }
        var trackerProperty = 'initiativepage';
        var turnOrderProperty = 'turnorder';
        
        this.trackerOpen = campaign().get(trackerProperty);

        this.add = function(_event, _index) {
            if (this.trackerOpen === false) {   // if the initiative tracker isn't open...
                return; // do nothing.
            }
            var turnOrder = this.fetch();
            var max = turnOrder.length;
            var min = -max;
            var atIndex = between(min, max).value(_index, 0);

            if (atIndex < 0) {
                atIndex += max;
            }
            
            turnOrder.splice(atIndex,0,_event);
            this.set(turnOrder);
        };
        
        this.remove = function(_at_index) {
            if (this.trackerOpen === false) {
                return;
            }
            
            var turnOrder = this.fetch();
            var max = turnOrder.length;
            var min = -max;
            var atIndex = between(min, max).value(_at_index, 0);
            
            if (atIndex < 0) {
                atIndex += max;
            }

            turnOrder.splice(atIndex,1);
            this.set(turnOrder);
        };
        
        this.set = function(_turn_order) {
            campaign().set(turnOrderProperty, JSON.stringify([].concat(_turn_order)));
        };
        
        this.fetch = function() {
            return [].concat(JSON.parse(campaign().get(turnOrderProperty)));
        };
        
        this.toString = function() {
            return campaign().get(turnOrderProperty);
        }
    }
    
    function TurnSequencerFactory(campiagn) {
        var sequencer = new TurnSequencer(campiagn);
        this.getTurnOrder = function() {
            return sequencer;
        };
    };
    
    
    on('chat:message', function(msg){
        if (/\(cm: v\d+\.\d+\.\d+\)/.test(msg.content) === true) {
            var registerCommand = state.CampaignManager.registerCommand;
            var factory = new TurnSequencerFactory(Campaign);
            var onWithCMUtils = state.CampaignManager.onWithCMUtils;
            
            registerCommand({
                name: 'add-effect',
                usage: '!!cm-add-effect (--name="Unknown" --duration="1" --increment="-1"|--round|--addmacros)',
                desc: 'Adds an effect to the end of the turn tracker. The name, duration, and increment values are optional, but if they are omitted the values presented in the usage will be used instead.<br><br>If the "round" option is used a round counter will be added to the end of the turn order.<br><br>The "addmacros" option will create a series of helper macros for you.',
                callback: function(options, utils, msg) {
                    var characterName;
                    var turnOrder = utils.guarantee(JSON.parse(Campaign().get('turnorder')),[]);
                    var effectName = utils.guarantee(options.name,'Unknown');
                    var effectDuration = parseInt(utils.guarantee(options.duration,'1'));
                    var effectIncrement = parseInt(utils.guarantee(options.increment,'-1'));
                    
                    if (Campaign().get('initiativepage') === false) {
                        utils.notify.GM('The turn order tracker must be active to use this command.');
                        return;
                    }
                    
                    var effect = { id: '-1' };
                    
                    if (_.has(options, 'addmacros') === true) {
                        var macros = findObjs({
                            _type: 'macro',
                            _playerid: msg.playerid
                        });
                        
                        var created = 0;
                        
                        _.each(defaultMacros, function(_macro){
                            var found = utils.guarantee(
                                findObjs({
                                    _type: 'macro',
                                    _playerid: msg.playerid,
                                    name: _macro.name
                                }),
                                []
                            );
                            
                            if (found.length === 0) {
                                createObj('macro',_.extendOwn(_macro,{_playerid: msg.playerid}));
                                created += 1;
                            }
                        });
                        
                        if (created > 0) {
                            utils.notify.GM('Some or all of the default macros have been created. You may wish to check the "In Bar" option for each.');
                        } else {
                            utils.notify.GM('The default macros have already been created.');
                        }
                        
                        return;
                    }
                    
                    if (_.has(options,'round') === true) {
                        
                        if (_.findWhere(turnOrder,{custom: 'Round'}) !== undefined) {
                            utils.notify.GM('A round counter is already present in the turn tracker.');
                            return;
                        }
                        
                        factory.getTurnOrder().add(
                            _.extendOwn(effect, {
                                custom: 'Round',
                                pr: 0,
                                formula: +1
                            })
                        );
                    } else {
                        factory.getTurnOrder().add(
                            _.extendOwn(effect, {
                                custom: utils.format("Effect: %s",effectName),
                                pr: effectDuration,
                                formula: effectIncrement
                            })
                        );
                    }
                },
                override: true,
                gmOnly: true
            });
            
            onWithCMUtils('change:campaign:turnorder', function(__, utils){
                var turnOrder = JSON.parse(Campaign().get('turnorder'));
                var eventEndMsg = '/desc <div style="background-color:red; color: white;font-variant:small-caps;">"%s" has ended.</div>'
                if (_.isArray(turnOrder) === false) {
                    return;
                }
                
                if (turnOrder.length === 0) {
                    return;
                }
                
                var recent = _.last(turnOrder);
                
                if (recent.custom.indexOf('Effect:') !== 0) {
                    return;
                }
                
                if (parseInt(recent.pr) < 1) {
                    factory.getTurnOrder().remove(-1);
                    sendChat(
                        'Event Tracker',
                        utils.format(eventEndMsg,recent.custom)
                    );
                    if (_.has(recent,'carryOver') === true) {
                        factory.getTurnOrder().add(recent.carryOver);
                    }
                }
            });
            
            onWithCMUtils('chat:message', function(msg, utils){
                var captures = {
                    aSpellHasBeenCast: /{{school=[^}]+}}/,
                    spellName: /{{name=([^}]+)}}/,
                    spellDuration: /{{duration=(?:\$\[\[)?(\d+)(?:\]\])?([^}]+)}}/,
                    timePerLevel: /\s?([^\/]+)\/?(level)?/i,
                    rounds: /r(?:ou)nds?/i,
                    minutes: /min(?:ute)s?/i,
                    hours: /h(?:our)s?/i,
                    casterLevel: /\(c(?:aster)?\s?l(?:evel)?:?\s*(\d+)\)/i,
                    characterName: /{{character_name=([^}]+)}}/,
                    castingTime: /{{casting_time=1\s*(?:standard|move)?\s+(action|round)\s*}}/i,
                    lingeringEffect: /\(l(?:ingering)?\s*e(?:ffect)?:?([^)]*)\)/i
                };
                
                utils.logU.debug('=================================================');
                utils.logU.debug('Capturing message for spell durations parsing');
                utils.logU.debug('=================================================');
                
                utils.logU.debug('Checking for turn tracker...');
                if (Campaign().get('initiativepage') === false) {
                    utils.logU.debug('Turn tracker is not active. Aborting...');
                    return;
                }
                utils.logU.debug('Turn tracker is active...');
                
                utils.logU.debug(
                    utils.format('Parsing current line: %s', msg.content)
                );
                
                utils.logU.debug('Checking for indications of a spell...');
                if (captures.aSpellHasBeenCast.test(msg.content) === false) {
                    utils.logU.debug('Couldn\'t find indications of a spell being cast');
                    utils.logU.debug(
                        utils.format('Current message captured: %s', msg.content)
                    );
                    return;
                }
                utils.logU.debug('Looks like a spell has been cast...');
                
                
                utils.logU.debug('Checking for casting time...');
                if (captures.castingTime.test(msg.content) === false) { // don't bother trying to record anything with a casting time of more than 1 round.
                    utils.logU.debug('Couldn\'t find a casting time. Aborting...');
                    return;
                }
                utils.logU.debug('Got a casting time...');
                
                var spellName = captures.spellName.exec(msg.content);
                var charName = captures.characterName.exec(msg.content);
                var duration = captures.spellDuration.exec(msg.content);
                var castingTime = captures.castingTime.exec(msg.content);
                
                if (spellName === null || duration === null) {
                    utils.notify.GM('Unable to parse the spell cast. Please enter in the event by hand');
                    return;
                }
                
                var time = parseInt(duration[1],10);
                var timeLength = captures.timePerLevel.exec(duration[2]);
                var multiple = 1;
                
                utils.logU.debug('Checking for spell duration...');
                if (timeLength === null) { // a spell that doesn't have a duration.
                    utils.logU.debug('Couldn\'t find a spell duration, duration is instantaneous, or duration greater than 1 round. Aborting...');
                    return;
                }
                utils.logU.debug('Good duration found...');
                
                if (timeLength.length === 3) {
                    var cl = captures.casterLevel.exec(msg.content);
                    if (cl !== null) {
                       time *= parseInt(cl[1]);
                    }
                }
                
                time *= (captures.hours.test(timeLength[1]))?(10 * 60):(captures.minutes.test(timeLength[1])?10:1);
                
                utils.logU.debug(factory.getTurnOrder().toString());
                
                if (castingTime[1] === 'round') {
                    factory.getTurnOrder().add({
                        id: '-1',
                        custom: utils.format(
                            'Effect: %s casting %s',charName[1], spellName[1]
                        ),
                        pr: 1,
                        formula: -1,
                        carryOver: {
                            id: '-1',
                            custom: utils.format(
                                'Effect: %s\'s %s', charName[1], spellName[1]
                            ),
                            pr: time,
                            formula: -1
                        }
                    });
                    return;
                }
                
                factory.getTurnOrder().add(
                    {
                        id: '-1',
                        custom: utils.format(
                            'Effect: %s %s', charName[1], spellName[1]
                        ),
                        pr: time,
                        formula: -1
                    }
                );
            });
        }
    })
}(state));