<a name"v0.3.0"></a>
## v0.3.0 (2016-12-11)


#### Other Changes

* updated code for the capture of the cast of spells: - the capture system will ig ([662758c2](/home/ubuntu/workspace/commit/662758c2))

<a name"v0.2.1"></a>
### v0.2.1 (2016-12-11)


#### Other Changes

* updated the code to use the v0.7.0 campaign manager by adding in the onWithCMUti ([e5855cf9](/home/ubuntu/workspace/commit/e5855cf9))

<a name"v0.2.0"></a>
## v0.2.0 (2016-12-11)


#### Other Changes

* forgot to add the main file to the branch. ([463d889b](/home/ubuntu/workspace/commit/463d889b))
* created a "singleton" to handle the updates of the turn tracker. this allows tha ([852dd1cd](/home/ubuntu/workspace/commit/852dd1cd))

<a name"v0.1.1"></a>
### v0.1.1 (2016-12-10)


#### Other Changes

* removed the sendChat() command warning if the Event Tracker was not open as this ([347d7124](/home/ubuntu/workspace/commit/347d7124))

<a name"v0.1.0"></a>
## v0.1.0 (2016-12-10)


#### Other Changes

* added a chat:message capture that will parse a spell block from the Pathfinder C ([29d81db2](/home/ubuntu/workspace/commit/29d81db2))
* Updated the script.json file with the new version (0.0.2). ([12018b11](/home/ubuntu/workspace/commit/12018b11))

<a name"v0.0.2"></a>
### v0.0.2 (2016-11-29)


#### Other Changes

* Minor change: updated the globals line to include Campaign, createObj, and sendC ([5233d65b](/home/ubuntu/workspace/commit/5233d65b))

